@extends('layout.frontoffice')
@section('content')
<h1><?php echo $h1;?></h1>
<hr/>
@if (session('status-success'))
<div class="alert alert-success">
	{{ session('status-success') }}
</div>
@endif
@if (session('status-error'))
<div class="alert alert-danger">
	{{ session('status-error') }}
</div>
@endif
@if (count($errors))
<div class="alert alert-danger">
	The form has errors
</div>
@endif

<?php 
$name = "";
if(old('name')!= ""){
	$name = old('name');
}else if (isset($company)){
	$name = $company->name;
}
?>

<br/>
<div class="row">
	<form action="{{ $action }}?>" id="form" method="post" role="form" data-toggle="validator">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="name">{{ trans('messages.name') }}</label>
			<input type="text" class="form-control" value="{{$name}}" name="name" id="name" placeholder="Name" data-minlength="3" required>

			<span class="help-block">{{ trans('messages.required_mininum_characters',['min'=>3]) }}</span>
			@if($errors->has('name'))
			<div class="help-block with-errors">
				{{ $errors->first('name') }}
			</div>
			@endif
		</div>
		<button type="submit" class="btn btn-primary">{{ trans('messages.save') }}</button>
	</form>
</div>
@endsection