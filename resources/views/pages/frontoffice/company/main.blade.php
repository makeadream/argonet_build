@extends('layout.frontoffice')
@section('content')
<h1>Companies</h1>
<hr/>
@can('lists-company')
<div class="row">
    <table id="companies_list" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Id</th>
                <th>Name</th>
            </tr>
        </tfoot>
        <tbody>
        </tbody>
    </table>
</div>
<script>
$(document).ready(function() {
    var table = $('#companies_list').DataTable({
        "ajax": "<?php echo url('api/company');?>",
        "columns": [
        { "data": "id" },
        { "data": "name" }
        ],
        "columnDefs": [
        { "targets": [0], "visible": false, "searchable": false},
        { "targets": [1], "visible": true , "searchable": true}
        ]
    });
    $('#companies_list tbody').on( 'click', 'tr', function () {
        var rowData = table.row( this ).data();
        window.location.href = "<?php echo url("company");?>/"+rowData.id;
    } );

} );
</script>
@endcan
@can('create-company')
<br/>
<div class="row-fluid">
    <a href="<?php echo url('company/form');?>" class="btn btn-primary">{{ trans('messages.create') }}</a>
</div>
@endcan


<script src="/js/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="/js/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
@endsection