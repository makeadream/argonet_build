@extends('layout.frontoffice')
@section('content')
<h1><?php echo $h1;?></h1>
<hr/>
@if (session('status-success'))
<div class="alert alert-success">
	{{ session('status-success') }}
</div>
@endif
@if (session('status-error'))
<div class="alert alert-danger">
	{{ session('status-error') }}
</div>
@endif
@if (count($errors))
<div class="alert alert-danger">
	The form has errors
</div>
@endif

<?php 
$name = "";
if(old('name')!= ""){
	$name = old('name');
}else if (isset($account)){
	$name = $account->name;
}

$manager_id = "";
if(old('manager_id')!= ""){
	$manager_id = old('manager_id');
}else if (isset($account)){
	$manager_id = $account->manager_id;
}

$company_id = "";
if(old('company_id')!= ""){
	$company_id = old('company_id');
}else if (isset($account)){
	$company_id = $account->company_id;
}

$classification_id = "";
if(old('account_classification_id')!= ""){
	$classification_id = old('account_classification_id');
}else if (isset($account)){
	$classification_id = $account->account_classification_id;
}
?>

<br/>
<div class="row">
	<form action="<?php echo $action;?>" id="form" method="post" role="form" data-toggle="validator">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="name">Name</label>
			<input type="text" class="form-control" value="{{$name}}" name="name" id="name" placeholder="Name" data-minlength="3" required>

			<span class="help-block">Minimum of 3 characters</span>
			@if($errors->has('name'))
			<div class="help-block with-errors">
				{{ $errors->first('name') }}
			</div>
			@endif
		</div>
		<div class="form-group">
			<label for="manager_id">Manager</label>
			<select class="form-control" name="manager_id" id="manager_id" required>
				<option value="">-</option>
				@foreach($managers as $manager)
					<option value="{{$manager->id}}" @if($manager->id == $manager_id) {{'selected'}} @endif>
						{{$manager->name}}
					</option>
				@endforeach
			</select>
			<span class="help-block">Required</span>
			@if($errors->has('manager_id'))
			<div class="help-block with-errors">
				{{ $errors->first('manager_id') }}
			</div>
			@endif
		</div>
		<div class="form-group">
			<label for="company_id">Company</label>
			<select class="form-control" name="company_id" id="company_id" required>
				<option value="">-</option>
				@foreach($companies as $company)
					<option value="{{$company->id}}" @if($company->id == $company_id) {{'selected'}} @endif>
						{{$company->name}}
					</option>
				@endforeach
			</select>
			<span class="help-block">Required</span>
			@if($errors->has('company_id'))
			<div class="help-block with-errors">
				{{ $errors->first('company_id') }}
			</div>
			@endif
		</div>
		<div class="form-group">
			<label for="company_id">Classification</label>
			<select class="form-control" name="account_classification_id" id="account_classification_id" required>
				<option value="">-</option>
				@foreach($classifications as $classification)
					<option value="{{$classification->id}}" @if($classification->id == $classification_id) {{'selected'}} @endif>
						{{$classification->name}}
					</option>
				@endforeach
			</select>
			<span class="help-block">Required</span>
			@if($errors->has('account_classification_id'))
			<div class="help-block with-errors">
				{{ $errors->first('account_classification_id') }}
			</div>
			@endif
		</div>
		<button type="submit" class="btn btn-primary">Save</button>
	</form>
</div>
@endsection