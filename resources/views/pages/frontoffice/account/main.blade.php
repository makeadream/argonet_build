@extends('layout.frontoffice')
@section('content')
<h1>Accounts</h1>
<hr/>
@can('lists-account')
<div class="row">
<table id="accounts_list" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Manager</th>
                <th>Classification</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Manager</th>
                <th>Classification</th>
            </tr>
        </tfoot>
        <tbody>
        </tbody>
    </table>
</div>
<script>
$(document).ready(function() {
    var table = $('#accounts_list').DataTable({
        "ajax": "<?php echo url('api/account');?>",
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "manager" },
            { "data": "classification" }
        ],
        "columnDefs": [
        { "targets": [0], "visible": false, "searchable": false},
        { "targets": [1], "visible": true , "searchable": true},
        { "targets": [2], "visible": true , "searchable": false},
        { "targets": [3], "visible": true , "searchable": false}
    ]
    });
    $('#accounts_list tbody').on( 'click', 'tr', function () {
        var rowData = table.row( this ).data();
        window.location.href = "<?php echo url("account");?>/"+rowData.id;
    } );
} );
</script>
@endcan
@can('create-account')
<br/>
    <div class="row-fluid">
    	<a href="<?php echo url('account/form');?>" class="btn btn-primary">{{ trans('messages.create') }}</a>
    </div>
    @endcan

<script src="/js/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="/js/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
@endsection