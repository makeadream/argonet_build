@extends('layout.frontoffice')
@section('content')
<h1><?php echo $h1;?></h1>
<hr/>
@if (session('status-success'))
<div class="alert alert-success">
	{{ session('status-success') }}
</div>
@endif
@if (session('status-error'))
<div class="alert alert-danger">
	{{ session('status-error') }}
</div>
@endif
@if (count($errors))
<div class="alert alert-danger">
	The form has errors
</div>
@endif

<?php 
$name = "";
if(old('name')!= ""){
	$name = old('name');
}else if (isset($project)){
	$name = $project->name;
}

$account_id = "";
if(old('account_id')!= ""){
	$account_id = old('account_id');
}else if (isset($project)){
	$account_id = $project->account_id;
}

$bu_id = "";
if(old('bu_id')!= ""){
	$name = old('bu_id');
}else if (isset($project)){
	$bussiness_unit_id = $project->bussiness_unit_id;
}
?>

<br/>
<div class="row">
	<form action="<?php echo $action;?>" id="form" method="post" role="form" data-toggle="validator">
		{{ csrf_field() }}
		<div class="form-group">
			<label for="name">{{ trans('messages.name') }}</label>
			<input type="text" class="form-control" value="{{$name}}" name="name" id="name" placeholder="Name" data-minlength="3" required>

			<span class="help-block">{{ trans('messages.required_mininum_characters',['min'=>3]) }}</span>
			@if($errors->has('name'))
			<div class="help-block with-errors">
				{{ $errors->first('name') }}
			</div>
			@endif
		</div>
		<div class="form-group">
			<label for="account_id">{{ trans('messages.account') }}</label>
			<select class="form-control" name="account_id" id="account_id" required>
				<option value="">-</option>
				@foreach($accounts as $account)
					<option value="{{$account->id}}" @if($account->id == $account_id) {{'selected'}} @endif>
						{{$account->name}}
					</option>
				@endforeach
			</select>
			<span class="help-block">{{ trans('messages.required') }}</span>
			@if($errors->has('account_id'))
			<div class="help-block with-errors">
				{{ $errors->first('account_id') }}
			</div>
			@endif
		</div>
		<div class="form-group">
			<label for="bussiness_unit_id">{{ trans('messages.bussiness_unit') }}</label>
			<select class="form-control" name="bussiness_unit_id" id="bussiness_unit_id" required>
				<option value="">-</option>
				@foreach($bus as $bu)
					<option value="{{$bu->id}}" @if($bu->id == $bu_id) {{'selected'}} @endif>
						{{$bu->name}}
					</option>
				@endforeach
			</select>
			<span class="help-block">{{ trans('messages.required') }}</span>
			@if($errors->has('bu_id'))
			<div class="help-block with-errors">
				{{ $errors->first('bu_id') }}
			</div>
			@endif
		</div>		
		<button type="submit" class="btn btn-primary">{{ trans('messages.save') }}</button>
	</form>
</div>
@endsection