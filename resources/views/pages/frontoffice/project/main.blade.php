@extends('layout.frontoffice')
@section('content')
<h1>Projects</h1>
<hr/>
@can('lists-project')
<div class="row">
    <table id="projects_list" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>BU</th>
                <th>Account</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>BU</th>
                <th>Account</th>
            </tr>
        </tfoot>
        <tbody>
        </tbody>
    </table>
</div>
<script>
$(document).ready(function() {
    var table = $('#projects_list').DataTable({
        "ajax": "<?php echo url('api/project');?>",
        "columns": [
        { "data": "id" },
        { "data": "name" }
        ],
        "columnDefs": [
        { "targets": [0], "visible": false, "searchable": false},
        { "targets": [1], "visible": true , "searchable": true},
        { "targets": [2], "visible": true, "searchable": false},
        { "targets": [3], "visible": true , "searchable": false}
        ]
    });
    $('#projects_list tbody').on( 'click', 'tr', function () {
        var rowData = table.row( this ).data();
        window.location.href = "<?php echo url("project/form");?>/"+rowData.id;
    } );

} );
</script>
@endcan
@can('create-project')
<br/>
<div class="row-fluid">
    <a href="<?php echo url('project/form');?>" class="btn btn-primary">{{ trans('messages.create') }}</a>
</div>
@endcan

<script src="/js/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="/js/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
@endsection