@extends('layout.frontoffice')
@section('content') 
<style>
html, body {
    height: 100%;
}
#wrapper {
    min-height: 100%;
    height: 100% !important; /* cross-browser */
}
#page-content-wrapper{
  height: 100% !important; 
  padding: 0;
}
.container-fluid{
  height: 100%;
  padding: 0;
}
</style>
<iframe src="{{ url('calendar/agenda') }}" width="100%" height="100%" frameborder="0"></iframe>

@endsection