@extends('layout.frontoffice')
@section('content')
<h1><?php echo trans('messages.validation_form_header');?></h1>
<hr/>
@if (session('status-success'))
<div class="alert alert-success">
	{{ session('status-success') }}
</div>
@endif
@if (session('status-error'))
<div class="alert alert-danger">
	{{ session('status-error') }}
</div>
@endif
@if (count($errors))
<div class="alert alert-danger">
	The form has errors
</div>
@endif


<br/>
<div class="row">
	<form action="<?php echo url('calendar/validationform');?>/@$event->id" id="form" method="post" role="form" data-toggle="validator">
		{{ csrf_field() }}
		<input type="hidden" name="sate" id="state"/>
		<div class="form-group">
			<label for="name">{{ trans('messages.subject') }}</label>
			<input type="text" readonly="readonly" class="form-control" value="{{$event->subject}}" name="subject" id="subject" placeholder="{{ trans('messages.subject') }}" >
		</div>

		<div class="form-group">
			<label for="client">{{ trans('messages.activity') }}</label>
			<input type="text" readonly="readonly" class="form-control" value="{{$event->activity()->name}}" name="activity" id="activity" placeholder="{{ trans('messages.activity') }}" >
		</div>

		<div class="form-group">
			<label for="client">{{ trans('messages.client') }}</label>
			<input type="text" readonly="readonly" class="form-control" value="{{$event->project()->account()->name}}" name="client" id="client" placeholder="{{ trans('messages.client') }}" >
		</div>

		<div class="form-group">
			<label for="client">{{ trans('messages.project') }}</label>
			<input type="text" readonly="readonly" class="form-control" value="{{$event->project()->name}}" name="project" id="project" placeholder="{{ trans('messages.project') }}" >
		</div>

		<div class="form-group">
			<label for="location">{{ trans('messages.location') }}</label>
			<input type="text" readonly="readonly" class="form-control" value="{{$event->location}}" name="location" id="location" placeholder="{{ trans('messages.location') }}" >
		</div>

		<div class="form-group">
			<label for="client">{{ trans('messages.initial_date') }}</label>
			<input type="text" readonly="readonly" class="form-control" value="{{$event->start_time}}" name="initial_date" id="initial_date" placeholder="{{ trans('messages.initial_date') }}" >
		</div>

		<div class="form-group">
			<label for="client">{{ trans('messages.end_date') }}</label>
			<input type="text" readonly="readonly" class="form-control" value="{{$event->end_time}}" name="end_date" id="end_date" placeholder="{{ trans('messages.end_date') }}" >
		</div>

		<button id="btn-validate" class="btn btn-success">{{ trans('messages.button_validate') }}</button>
		<butotn id="btn-no-validate" class="btn btn-danger">{{ trans('messages.button_no_validate') }}</button>
	</form>
</div>

<script>
$(document).ready(function (){
	$('#btn-validate').click(function (){
		$('#state').val('VALIDATED');
		$('#form').submit();
	});
	$('#btn-no-validate').click(function (){
		$('#state').val('NOT_VALIDATED');
		$('#form').submit();
	});
});
</script>

@endsection