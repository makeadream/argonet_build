<?php
$project_id = "";
if(old('project_id')!= ""){
  $project_id = old('project_id');
}else if (isset($event)){
  $project_id = $event->project_id;
}

$activity_id = "";
if(old('activity_id')!= ""){
  $activity_id = old('activity_id');
}else if (isset($event)){
  $activity_id = $event->activity_id;
}

if(old('account_id')!= ""){
  $account_id = old('account_id');
}else if(!isset($account_id)){
  $account_id ="";
}

$subject = "";
if(old('subject')!= ""){
  $subject = old('subject');
}else if (isset($event)){
  $subject = $event->subject;
}

$description = "";
if(old('description')!= ""){
  $description = old('description');
}else if (isset($event)){
  $description = $event->description;
}

$location = "";
if(old('location')!= ""){
  $location = old('location');
}else if (isset($event)){
  $location = $event->location;
}

$color = "";
if(old('color')!= ""){
  $color = old('color');
}else if (isset($event)){
  $color = $event->color;
}

$timezone = "";
if(old('timezone')!= ""){
  $timezone = old('timezone');
}else if (isset($event)){
  $timezone = $event->timezone;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>    
  <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">    
  <title>Calendar Details</title>    
  <link href="/css/main.css" rel="stylesheet" type="text/css" />       
  <link href="/css/dp.css" rel="stylesheet" />    
  <link href="/css/dropdown.css" rel="stylesheet" />    
  <link href="/css/colorselect.css" rel="stylesheet" />   
  
  <script src="/js/jquery.js" type="text/javascript"></script>    
  <script src="/js/Plugins/Common.js" type="text/javascript"></script>        
  <script src="/js/Plugins/jquery.form.js" type="text/javascript"></script>     
  <script src="/js/Plugins/jquery.validate.js" type="text/javascript"></script>     
  <script src="/js/Plugins/datepicker_lang_US.js" type="text/javascript"></script>        
  <script src="/js/Plugins/jquery.datepicker.js" type="text/javascript"></script>     
  <script src="/js/Plugins/jquery.dropdown.js" type="text/javascript"></script>     
  <script src="/js/Plugins/jquery.colorselect.js" type="text/javascript"></script>    
  
  <script type="text/javascript">
  if (!DateAdd || typeof (DateDiff) != "function") {
    var DateAdd = function(interval, number, idate) {
      number = parseInt(number);
      var date;
      if (typeof (idate) == "string") {
        date = idate.split(/\D/);
        eval("var date = new Date(" + date.join(",") + ")");
      }
      if (typeof (idate) == "object") {
        date = new Date(idate.toString());
      }
      switch (interval) {
        case "y": date.setFullYear(date.getFullYear() + number); break;
        case "m": date.setMonth(date.getMonth() + number); break;
        case "d": date.setDate(date.getDate() + number); break;
        case "w": date.setDate(date.getDate() + 7 * number); break;
        case "h": date.setHours(date.getHours() + number); break;
        case "n": date.setMinutes(date.getMinutes() + number); break;
        case "s": date.setSeconds(date.getSeconds() + number); break;
        case "l": date.setMilliseconds(date.getMilliseconds() + number); break;
      }
      return date;
    }
  }
  function getHM(date)
  {
   var hour =date.getHours();
   var minute= date.getMinutes();
   var ret= (hour>9?hour:"0"+hour)+":"+(minute>9?minute:"0"+minute) ;
   return ret;
 }
 $(document).ready(function() {
            //debugger;
            var DATA_FEED_URL = "/calendar/saveevent";
            var arrT = [];
            var tt = "{0}:{1}";
            for (var i = 0; i < 24; i++) {
              arrT.push({ text: StrFormat(tt, [i >= 10 ? i : "0" + i, "00"]) }, { text: StrFormat(tt, [i >= 10 ? i : "0" + i, "30"]) });
            }
            $("#timezone").val(new Date().getTimezoneOffset()/60 * -1);
            $("#stparttime").dropdown({
              dropheight: 200,
              dropwidth:60,
              selectedchange: function() { },
              items: arrT
            });
            $("#etparttime").dropdown({
              dropheight: 200,
              dropwidth:60,
              selectedchange: function() { },
              items: arrT
            });
            var check = $("#IsAllDayEvent").click(function(e) {
              if (this.checked) {
                $("#stparttime").val("00:00").hide();
                $("#etparttime").val("00:00").hide();
              }
              else {
                var d = new Date();
                var p = 60 - d.getMinutes();
                if (p > 30) p = p - 30;
                d = DateAdd("n", p, d);
                $("#stparttime").val(getHM(d)).show();
                $("#etparttime").val(getHM(DateAdd("h", 1, d))).show();
              }
            });
            if (check[0].checked) {
              $("#stparttime").val("00:00").hide();
              $("#etparttime").val("00:00").hide();
            }
            $("#Savebtn").click(function() { $("#fmEdit").submit(); });
            $("#Closebtn").click(function() { CloseModelWindow(); });
            $("#Deletebtn").click(function() {
             if (confirm("Are you sure to remove this event")) {  
              var param = [{ "name": "calendarId", value: $('#event_id').val()}];                
              $.post("<?php echo url('api/event');?>" + "?method=remove",
                param,
                function(data){
                  if (data.IsSuccess) {
                    alert(data.Msg); 
                    CloseModelWindow(null,true);                            
                  }
                  else {
                    alert("Error occurs.\r\n" + data.Msg);
                  }
                }
                ,"json");
            }
          });
            
            $("#stpartdate,#etpartdate").datepicker({ picker: "<button class='calpick'></button>"});    
            var cv =$("#colorvalue").val() ;
            if(cv=="")
            {
              cv="-1";
            }
            $("#calendarcolor").colorselect({ title: "Color", index: cv, hiddenid: "colorvalue" });
            //to define parameters of ajaxform
            var options = {
              beforeSubmit: function() {
                return true;
              },
              dataType: "json",
              success: function(data) {
                alert(data.Msg);
                if (data.IsSuccess) {
                  CloseModelWindow(null,true);  
                }
              }
            };
            $.validator.addMethod("date", function(value, element) {                             
              var arrs = value.split(i18n.datepicker.dateformat.separator);
              var year = arrs[i18n.datepicker.dateformat.year_index];
              var month = arrs[i18n.datepicker.dateformat.month_index];
              var day = arrs[i18n.datepicker.dateformat.day_index];
              var standvalue = [year,month,day].join("-");
              return this.optional(element) || /^(?:(?:1[6-9]|[2-9]\d)?\d{2}[\/\-\.](?:0?[1,3-9]|1[0-2])[\/\-\.](?:29|30))(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?: \d{1,3})?)?$|^(?:(?:1[6-9]|[2-9]\d)?\d{2}[\/\-\.](?:0?[1,3,5,7,8]|1[02])[\/\-\.]31)(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?: \d{1,3})?)?$|^(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])[\/\-\.]0?2[\/\-\.]29)(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?: \d{1,3})?)?$|^(?:(?:16|[2468][048]|[3579][26])00[\/\-\.]0?2[\/\-\.]29)(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?: \d{1,3})?)?$|^(?:(?:1[6-9]|[2-9]\d)?\d{2}[\/\-\.](?:0?[1-9]|1[0-2])[\/\-\.](?:0?[1-9]|1\d|2[0-8]))(?: (?:0?\d|1\d|2[0-3])\:(?:0?\d|[1-5]\d)\:(?:0?\d|[1-5]\d)(?:\d{1,3})?)?$/.test(standvalue);
            }, "Invalid date format");
$.validator.addMethod("time", function(value, element) {
  return this.optional(element) || /^([0-1]?[0-9]|2[0-3]):([0-5][0-9])$/.test(value);
}, "Invalid time format");
$.validator.addMethod("safe", function(value, element) {
  return this.optional(element) || /^[^$\<\>]+$/.test(value);
}, "$<> not allowed");
$("#fmEdit").validate({
  submitHandler: function(form) { $("#fmEdit").ajaxSubmit(options); },
  errorElement: "div",
  errorClass: "cusErrorPanel",
  errorPlacement: function(error, element) {
    showerror(error, element);
  }
});
function showerror(error, target) {
  var pos = target.position();
  var height = target.height();
  var newpos = { left: pos.left, top: pos.top + height + 2 }
  var form = $("#fmEdit");             
  error.appendTo(form).css(newpos);
}
});
</script>      
<style type="text/css">     
.calpick     {        
  width:16px;   
  height:16px;     
  border:none;        
  cursor:pointer;        
  background:url("/css/images/cal.gif") no-repeat center 2px;        
  margin-left:-22px;    
}     
</style>
</head>
<body>    
  <div>      
    <div class="toolBotton">           
      <a id="Savebtn" class="imgbtn" href="javascript:void(0);">                
        <span class="Save"  title="Save the calendar">Save(<u>S</u>)
        </span>          
      </a>                           
      @if(isset($event))
      <a id="Deletebtn" class="imgbtn" href="javascript:void(0);">                    
        <span class="Delete" title="Cancel the calendar">Delete(<u>D</u>)
        </span>                
      </a>             
      @endif            
      <a id="Closebtn" class="imgbtn" href="javascript:void(0);">                
        <span class="Close" title="Close the window" >Close
        </span></a>            
      </a>        
    </div>                  
    <div style="clear: both">         
    </div>        
    <div class="infocontainer">       

      <form action="{{$url}}" class="fform" id="fmEdit" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="event_id" id="event_id" value="@if(isset($event)) {{$event->id}} @endif"></input>
        <table>
          <tr>
            <td>Account:</td>
            <td>Project:</td>
            <td>Activity:</td>
          </tr>
          <tr>
            <td>
              <select id="Account" name="Account"  >
                <option value="0">-</option>
                @foreach($accounts as $account)
                  <option value="{{$account->id}}" @if($account->id == $account_id) {{'selected'}} @endif >{{$account->name}}</option>
                @endforeach
              </select>
            </td>
            <td>
              <select id="Project" name="Project"  >
                <option value="0">-</option>
                @foreach($projects as $project)
                  <option value="{{$project->id}}" @if($project->id == $project_id) {{'selected'}} @endif >{{$project->name}}</option>
                @endforeach
              </select>
            </td>
            <td>
              <select id="Activity" name="Activity"  >
                <option value="0">-</option>
                @foreach($activities as $activity)
                  <option value="{{$activity->id}}" @if($activity->id == $activity_id) {{'selected'}} @endif>{{$activity->name}}</option>
                @endforeach
              </select>
            </td>
          </tr>
        </table>                 
        <label>                    
          <span>*Subject:</span>                    
          <input MaxLength="200" class="required safe" id="Subject" name="Subject" style="width:85%;" type="text" value="{{$subject}}" />                     
          <div id="calendarcolor">
          </div>
          <input id="colorvalue" name="colorvalue" type="hidden" value="{{$color}}" />                
        </label>                 
        <label>                    
          <span>*Time:</span>                    
          <div>  
            <input MaxLength="10" class="required date" id="stpartdate" name="stpartdate" style="padding-left:2px;width:90px;" type="text" value="<?php echo isset($event)?$event->getStartDateInJS():""; ?>" />                       
            <input MaxLength="5" class="required time" id="stparttime" name="stparttime" style="width:40px;" type="text" value="<?php echo isset($event)?$event->getStartTimeInJS():""; ?>" />To                       
            <input MaxLength="10" class="required date" id="etpartdate" name="etpartdate" style="padding-left:2px;width:90px;" type="text" value="<?php echo isset($event)?$event->getEndDateInJS():""; ?>" />                       
            <input MaxLength="50" class="required time" id="etparttime" name="etparttime" style="width:40px;" type="text" value="<?php echo isset($event)?$event->getEndTimeInJS():""; ?>" />                                            
            <label class="checkp"> 
              <input id="IsAllDayEvent" name="IsAllDayEvent" type="checkbox" value="1" @if(isset($event)&&$event->all_day!=0) {{"checked"}} @endif />          All Day Event                      
            </label>                    
          </div>                
        </label> 
        <label>                    
          <span>Location:</span>                    
          <input MaxLength="200" id="Location" name="Location" style="width:95%;" type="text" value="{{$location}}" />                 
        </label>                 
        <label>                    
          <span>Description:</span>                    
          <textarea cols="20" id="Description" name="Description" rows="2" style="width:95%; height:70px">{{$description}}</textarea>                
        </label>                
        <input id="timezone" name="timezone" type="hidden" value="{{$timezone}}" />           
      </form>         
    </div>         
  </div>
  <script>
  $(document).ready(function (){
    $('#Account').change(function(){
      var accountList = $(this);
      $.ajax({
        url: "<?php echo url('api/project');?>?account_id="+$(this).val(),
        success: function (response) {
          if(typeof(response.data) != "undefined" && response.data !== null){
            $('#Project').find('option').remove();
            $.each(response.data,function (index,obj){
              $('#Project').append('<option value="'+obj.id+'">'+obj.name+'</option>');
            });
          }else{
            alert('Error on reading projects');
          }
        },
        error: function (xhr, ajaxOptions, thrownError) {
          alert('Error on getting projects');
        }
      });
    });
  });
  </script>
</body>
</html>