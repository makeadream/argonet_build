@extends('layout.frontoffice')
@section('content')
<h1>Companies</h1>
<hr/>
@if (session('status-success'))
<div class="alert alert-success">
    {{ session('status-success') }}
</div>
@endif
@if (session('status-error'))
<div class="alert alert-danger">
    {{ session('status-error') }}
</div>
@endif
@if (count($errors))
<div class="alert alert-danger">
    The form has errors
</div>
@endif

<br/>
<div class="row">
<table id="event_list" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%" role="grid" aria-describedby="example_info" style="width: 100%;">
        <thead>
            <tr>
                <th>Id</th>
                <th>Subject</th>
                <th>Project</th>
                <th>Account</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Id</th>
                <th>Subject</th>
                <th>Project</th>
                <th>Account</th>
            </tr>
        </tfoot>
        <tbody>
        </tbody>
    </table>
</div>
<br/>

<script>
$(document).ready(function() {
    var table = $('#event_list').DataTable({
        "ajax": "<?php echo url('api/eventvalidation');?>",
        "columns": [
            { "data": "id" },
            { "data": "subject" },
            { "data": "project" },
            { "data": "account" },
        ],
        "columnDefs": [
            { "targets": [0], "visible": false, "searchable": false},
            { "targets": [1], "visible": true , "searchable": true},
            { "targets": [2], "visible": true , "searchable": false},
            { "targets": [3], "visible": true , "searchable": false}
        ]
    });
    $('#companies_list tbody').on( 'click', 'tr', function () {
        var rowData = table.row( this ).data();
        window.location.href = "<?php echo url('calendar/validationform');?>/"+rowData.id;
    } );

} );
</script>
<script src="/js/datatables/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="/js/datatables/dataTables.bootstrap.min.js" type="text/javascript"></script>
@endsection