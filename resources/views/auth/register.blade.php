@extends('layout.default')
@section('content')

<style>

.card-container.card {
    max-width: 350px;
    padding: 40px 40px;
}

.btn {
    font-weight: 700;
    height: 36px;
    -moz-user-select: none;
    -webkit-user-select: none;
    user-select: none;
    cursor: default;
}

/*
 * Card component
 */
.card {
    background-color: #F7F7F7;
    /* just in case there no content*/
    padding: 20px 25px 30px;
    margin: 0 auto 25px;
    margin-top: 50px;
    /* shadows and rounded borders */
    -moz-border-radius: 2px;
    -webkit-border-radius: 2px;
    border-radius: 2px;
    -moz-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    -webkit-box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
    box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
}

.profile-img-card {
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}

/*
 * Form styles
 */
.profile-name-card {
    font-size: 16px;
    font-weight: bold;
    text-align: center;
    margin: 10px 0 0;
    min-height: 1em;
}

.reauth-email {
    display: block;
    color: #404040;
    line-height: 2;
    margin-bottom: 10px;
    font-size: 14px;
    text-align: center;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
}

.form-register #inputEmail,
.form-register #inputPassword,
.form-register #inputPasswordConfirmation,
.form-register #inputName,
.form-register #selectRole
 {
    direction: ltr;
    height: 44px;
    font-size: 16px;
}

.form-register input[type=email],
.form-register input[type=password],
.form-register input[type=text],
.form-register select,
.form-register button {
    width: 100%;
    display: block;
    margin-bottom: 10px;
    z-index: 1;
    position: relative;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
}

.form-register .form-control:focus {
    border-color: rgb(104, 145, 162);
    outline: 0;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075),0 0 8px rgb(104, 145, 162);
}




.forgot-password {
    color: rgb(104, 145, 162);
}

.forgot-password:hover,
.forgot-password:active,
.forgot-password:focus{
    color: rgb(12, 97, 33);
}
.btn-login{
    visibility: hidden;
}

footer
{
display: none;
}
</style>

<div class="container container-login">
    <div class="card card-container">
        <img id="profile-img" class="profile-img-card" src="/images/avatar.png" />
        <p id="profile-name" class="profile-name-card"></p>
        <form class="form-register" method="post" action="/auth/register">
            {!! csrf_field() !!}
            @if (count($errors))
                <div class="alert alert-danger">
                {!! trans('messages.register_error') !!}
                </div>
            @endif
        <input type="text" name="name" value="{{ old('name') }}" id="inputName" class="form-control" placeholder="{!! trans('messages.name') !!}" required autofocus>
        
            @if($errors->has('name'))
            <div class="help-block with-errors">
                {{ $errors->first('name') }}
            </div>
            @endif
        <input type="email" name="email" value="{{ old('email') }}" id="inputEmail" class="form-control" placeholder="{!! trans('messages.email_address') !!}" required>
        
            @if($errors->has('email'))
            <div class="help-block with-errors">
                {{ $errors->first('email') }}
            </div>
            @endif 


        <input type="password" name="password" id="inputPassword" class="form-control" placeholder="{!! trans('messages.password') !!}" required>
        
        @if($errors->has('password'))
            <div class="help-block with-errors">
                {{ $errors->first('password') }}
            </div>
            @endif

        <input type="password" name="password_confirmation"  id="inputPasswordConfirmation" class="form-control" placeholder="{!! trans('messages.password_confirmation') !!}" required>
        
            @if($errors->has('password_confirmation'))
            <div class="help-block with-errors">
                {{ $errors->first('password_confirmation') }}
            </div>
            @endif
            <button class="btn btn-primary" type="submit" >{!! trans('messages.register') !!}</button>
        </form><!-- /form -->

    </div><!-- /card-container -->
</div><!-- /container -->

@endsection