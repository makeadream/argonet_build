

<!-- Sidebar -->
<div id="sidebar-wrapper">
	<ul class="sidebar-nav">
		<li class="sidebar-brand">
			<a href="{{ url('dashboard')}}">
				ArgoNet - CRM
			</a>
		</li>
		@can('menu-dashboard')
		<li>
			<a href="{{ url('dashboard')}}">{{ trans('messages.dashboard')}}</a>
		</li>
		@endcan
		@can('menu-company')
		<li>
			<a href="{{ url('company')}}">{{ trans('messages.companies')}}</a>
		</li>
		@endcan
		@can('menu-account')
			<li>
				<a href="{{ url('account')}}">{{ trans('messages.accounts')}}</a>
			</li>
		@endcan
		@can('menu-project')
		<li>
			<a href="{{ url('project')}}">{{ trans('messages.projects')}}</a>
		</li>
		@endcan
		@can('menu-calendar')
		<li>
			<a href="{{ url('calendar')}}">{{ trans('messages.calendar')}}</a>
		</li>
		@endcan
	</ul>
</div>

