<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1">
	@include('layout.header')
</head>
<body>
	<div id="wrapper">
		@include('layout.leftmenu')
		<div id="page-content-wrapper">
			<div class="container-fluid">
				@yield('content')
			</div>
		</div>
		@include('layout.footer')
	</div>
</body>
</html>