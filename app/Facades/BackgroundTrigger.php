<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class BackgroundTrigger extends Facade{
    protected static function getFacadeAccessor() { return 'BackgroundTrigger'; }
}