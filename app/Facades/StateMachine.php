<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class StateMachine extends Facade{
    protected static function getFacadeAccessor() { return 'StateMachine'; }
}