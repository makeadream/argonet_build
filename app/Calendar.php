<?php

namespace App;

class Calendar{

	public static function addCalendar($st, $et, $sub, $ade){
		$ret = array();
		$ret['IsSuccess'] = true;
		$ret['Msg'] = 'add success';
		$ret['Data'] =rand();
		return $ret;
	}

	public static function addDetailedCalendar($st, $et, $sub, $ade, $dscr, $loc, $color, $tz){
		$ret = array();
		$ret['IsSuccess'] = true;
		$ret['Msg'] = 'add success';
		$ret['Data'] = rand();
		return $ret;
	}

	public static function listCalendarByRange($sd, $ed, $cnt){
		$ret = array();
		$ret['events'] = array();
		$ret["issort"] =true;
		$ret["start"] = self::php2JsTime($sd);
		$ret["end"] = self::php2JsTime($ed);
		$ret['error'] = null;
		$title = array('team meeting', 'remote meeting', 'project plan review', 'annual report', 'go to dinner');
		$location = array('Lodan', 'Newswer', 'Belion', 'Moore', 'Bytelin');
		for($i=0; $i<$cnt; $i++) {
			$rsd = rand($sd, $ed);
			$red = rand(3600, 10800);
			if(rand(0,10) > 8){
				$alld = 1;
			}else{
				$alld=0;
			}
			$ret['events'][] = array(rand(10000, 99999),$title[rand(0,4)],self::php2JsTime($rsd),self::php2JsTime($red),rand(0,1),
        $alld, //more than one day event
        0,//Recurring event
        rand(-1,13),
        1, //editable
        $location[rand(0,4)], 
        ''//$attends
        );
		}
		return $ret;
	}

	public static function listCalendar($day, $type){
		$phpTime = self::js2PhpTime($day);
		switch($type){
			case "month":
			$st = mktime(0, 0, 0, date("m", $phpTime), 1, date("Y", $phpTime));
			$et = mktime(0, 0, -1, date("m", $phpTime)+1, 1, date("Y", $phpTime));
			$cnt = 50;
			break;
			case "week":
			$monday  =  date("d", $phpTime) - date('N', $phpTime) + 1;
			$st = mktime(0,0,0,date("m", $phpTime), $monday, date("Y", $phpTime));
			$et = mktime(0,0,-1,date("m", $phpTime), $monday+7, date("Y", $phpTime));
			$cnt = 20;
			break;
			case "day":
			default:
			$st = mktime(0, 0, 0, date("m", $phpTime), date("d", $phpTime), date("Y", $phpTime));
			$et = mktime(0, 0, -1, date("m", $phpTime), date("d", $phpTime)+1, date("Y", $phpTime));
			$cnt = 5;
			break;
		}		
		return ['start_time' => $st, 'end_time' => $et];
		//return self::listCalendarByRange($st, $et, $cnt);
	}

	public static function updateCalendar($id, $st, $et){
		$ret = array();
		$ret['IsSuccess'] = true;
		$ret['Msg'] = 'Succefully';
		return $ret;
	}

	public static function updateDetailedCalendar($id, $st, $et, $sub, $ade, $dscr, $loc, $color, $tz){
		$ret = array();
		$ret['IsSuccess'] = true;
		$ret['Msg'] = 'Succefully';
		return $ret;
	}

	public static function removeCalendar($id){
		$ret = array();
		$ret['IsSuccess'] = true;
		$ret['Msg'] = 'Succefully';
		return $ret;
	}

	public static function js2PhpTime($jsdate){
		if(preg_match('@(\d+)/(\d+)/(\d+)\s+(\d+):(\d+)@', $jsdate, $matches)==1){
			$ret = mktime($matches[4], $matches[5], 0, $matches[1], $matches[2], $matches[3]);
		}else if(preg_match('@(\d+)/(\d+)/(\d+)@', $jsdate, $matches)==1){
			$ret = mktime(0, 0, 0, $matches[1], $matches[2], $matches[3]);
		}else{
			$ret = mktime(0,0,0,date('m'),date('d'),date('Y'));
		}


		return $ret;
	}

	public static function js2MySql($jsdate){
		$phpdate = self::js2PhpTime($jsdate);
		$mysqldate = self::php2MySqlTime($phpdate);
		return $mysqldate;
	}

	public static function Mysql2Js($mysqldate){
		$phpdate = self::mySql2PhpTime($mysqldate);
		$jsdate = self::php2JsTime($phpdate);
		return $jsdate;
	}

	public static function php2JsTime($phpDate){
		return date("m/d/Y H:i", $phpDate);
	}

	public static function php2MySqlTime($phpDate){
		return date("Y-m-d H:i:s", $phpDate);
	}

	public static function mySql2PhpTime($sqlDate){
		$arr = date_parse($sqlDate);
		return mktime($arr["hour"],$arr["minute"],$arr["second"],$arr["month"],$arr["day"],$arr["year"]);

	}

}
