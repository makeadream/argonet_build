<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

/* Dedicated Routes for Microsoft Live Authetication */
//Route::get('/login', 'Auth\AuthController@redirectToProvider');
//Route::get('/login/callback', 'Auth\AuthController@handleProviderCallback');

Route::get('dashboard', [ 'as' =>'dashboard', 'uses' => 'CRMController@dashboard']);
Route::get('calendar', [ 'uses' => 'CalendarController@main']);
Route::get('company', [ 'uses' => 'CompanyController@main']);
Route::get('account', [ 'uses' => 'AccountController@main']);
Route::get('project', [ 'uses' => 'ProjectController@main']);

Route::group(['prefix' => 'project'], function () {
	Route::get('/{id?}', [ 'as' => 'getproject', 'uses' => 'ProjectController@form']);
	Route::post('/{id?}', [ 'as' => 'postproject', 'uses' => 'ProjectController@save']);
	Route::get('list', [ 'uses' => 'ProjectController@list']);
});

Route::group(['prefix' => 'company'], function () {
	Route::get('/{id}', [ 'as' => 'getcompany', 'uses' => 'CompanyController@form']);
	Route::post('/{id?}', [ 'as' => 'postcompany', 'uses' => 'CompanyController@save']);
	Route::get('list', [ 'uses' => 'CompanyController@list']);
});

Route::group(['prefix' => 'account'], function () {
	Route::get('/{id?}', [ 'as' => 'getaccount', 'uses' => 'AccountController@form']);
	Route::post('/{id?}', [ 'as' => 'postaccount', 'uses' => 'AccountController@save']);
	Route::get('list', [ 'uses' => 'AccountController@list']);
});

Route::group(['prefix' => 'calendar'], function () {
	Route::get('agenda', [ 'uses' => 'CalendarController@index']);
	Route::get('validation', [ 'uses' => 'CalendarController@validationlist']);
	Route::get('validationform', [ 'uses' => 'CalendarController@validationform']);
	Route::get('event/{id?}', [ 'uses' => 'CalendarController@event']);
	Route::post('event/{id?}', ['uses' => 'CalendarController@saveevent']);
});

Route::group(['prefix' => 'api'], function () {
	Route::get('eventvalidation', [ 'uses' => 'CalendarController@apivalidationlist']);
	Route::post('calendar', [ 'uses' => 'CalendarController@calendar']);
	Route::post('event', [ 'uses' => 'CalendarController@quickeventaction']);
	Route::get('account', [ 'uses' => 'AccountController@apilist']);
	Route::get('company', [ 'uses' => 'CompanyController@apilist']);
	Route::get('project', [ 'uses' => 'ProjectController@apilist']);
});


Route::get('/', [
    'middleware' => 'auth',
	'uses' => 'CRMController@dashboard'
]);
