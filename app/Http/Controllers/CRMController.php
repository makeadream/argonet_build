<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Models\Client;
use App\Models\Activity;
use App\Models\Event;
use App\Models\Project;
use DB;
use Gate;
use Auth;


class CRMController extends Controller
{
 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {

        if(!$this->authorize('menu-dashboard')){
            abort(403);
        }

        return View::make('pages.frontoffice.crm');
    }


}
