<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Company;
use View;
use Validator;
use Session;

class CompanyController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function main()
    {
        if(!$this->authorize('menu-company')){
            abort(403);
        }

        return View::make('pages.frontoffice.company.main');
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function form($id = 0)
    {
        if(!$this->authorize('form',new Company)){
            abort(403);
        }
        
        $data = [   
        'action' => url('company'),
        'h1' => 'New Company'
        ];

        if(0 != $id){

            $company = Company::find($id);
            if(!$company){
                Session::flash('status-error', trans('messages.no_object',['object'=>'Company']));
                return View::make('pages.frontoffice.company.form',$data);
            }

            if(!$this->authorize('read',$company)){
                Session::flash('status-error', trans('messages.no_permission'));
                return View::make('pages.frontoffice.company.form',$data);
            }     


            $data = [   
            'action' => url('company/'.$id),
            'company' => $company,
            'h1' => 'Change Company'
            ];
        }

        return View::make('pages.frontoffice.company.form',$data);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request, $id=0)
    {


        $data = [];
        
        //Verificar se veio ou não um valor no parametro $id
        if(0 == $id){

            $validator = Validator::make(
                array('name' => $request->input('name')),
                array('name'=> 'required|min:3'),
                array('name' => 'Name is not valid')
                );

            if($validator->fails()){
                return redirect('company')->withErrors($validator);
            }

            if(!$this->authorize('create',new Company)){
                abort(403);
            }  

            try{

                Company::create([
                    'name' => $request->input('name'),
                    'created_by' => $this->user()->id
                    ]);
            }catch(Exception $e){
                return redirect()->route('getcompany',[$id])->with('status-error', trans('messages.db_error'));     
            }

            return redirect()->route('getcompany',[$id])->with('status-success', trans('messages.object_created',['object'=>'Company'])); 
        }

        $validator = Validator::make(
            array('id' => $id,
                'name' => $request->input('name')),
            array('id' => 'required|unique:companies,id,'.$id,
                'name'=> 'required|min:3'),
            array(
                'id' => 'The company does not exists',
                'name' => 'Name is not valid'
                ));

        if($validator->fails()){
            return redirect()->route('getcompany',[$id])->withErrors($validator)->withInput($request->all());
        }

       
        try{
            $company = Company::find($id);
            if(!$this->authorize('update',$company)){
                abort(403);
            }  
            $company->name = $request->input('name');
            $company->created_by = $this->user()->id;
            $company->save();

        }catch(Exception $e){
            return redirect()->route('getcompany',[$id])->with('status-error', trans('messages.db_error'))->withInput($request->all());     
        }
        
        return redirect()->route('getcompany',[$id])->with('status-success', trans('messages.object_created',['object'=>'Company']));   
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function apilist()
    {
        if(!$this->authorize('lists',new Company)){
            abort(403);
        }  

        $ret['data'] = Company::select('id','name')->get();
        $ret['draw'] = 1;
        $ret['recordsTotal'] = count($ret['data']);
        $ret['recordsFiltered'] = count($ret['data']);

        return response()->json($ret);
    }
}
