<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Project;
use View;
use App\Models\Account;
use Validator;

class ProjectController extends Controller
{


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function main()
    {
        if(!$this->authorize('menu-project')){
            abort(403);
        }

        return View::make('pages.frontoffice.project.main');
    }

      /**
     * Display a listing of the resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
      public function form($id = 0)
      {

        if(!$this->authorize('form',new Project)){
            abort(403);
        }

        $data = [   
        'action' => url('project'),
        'h1' => 'New Project'
        ];

        if(0 != $id){
            $project = Project::find($id);

            if(!$project){
                Session::flash('status-error', trans('messages.no_object',['object' => 'Project']));
                return View::make('pages.frontoffice.project.form',$data);
            }

            if(!$this->authorize('read',$project)){
                Session::flash('status-error', trans('messages.no_permission'));
                return View::make('pages.frontoffice.project.form',$data);
            }

            $data = [   
            'action' => url('project/'.$id),
            'project' => $project,
            'h1' => 'Change Project'
            ];
        }

        $data['accounts'] = Account::all();

        return View::make('pages.frontoffice.project.form',$data);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request, $id=0)
    {


        $data = [];
        
        //Verificar se veio ou não um valor no parametro $id
        if(0 == $id){

            $validator = Validator::make(
                array('account_id' => $request->input('account_id'),
                    'name' => $request->input('name')),
                array(
                    'account_id' => 'required|exists:accounts,id',
                    'bussiness_unit_id' => 'required|exists:bussiness_unit,id',
                    'name'=> 'required|min:3'
                    ));

            if($validator->fails()){
                return redirect('project')->withErrors($validator);
            }

            
            if(!$this->authorize('create',new Project)){
                abort(403);
            }

            try{
                Project::create([
                    'name' => $request->input('name'),
                    'created_by' => $user_id,
                    'account_id' => $request->input('account_id'),
                    'bussiness_unit_id' => $request->input('bussiness_unit_id'),
                    ]);
            }catch(Exception $e){
                return redirect()->route('getproject',[$id])->with('status-error', trans('messages.db_error'));     
            }

            return redirect()->route('getproject',[$id])->with('status-success', trans('messages.object_created',['object'=>'Project'])); 
        }

        $validator = Validator::make(
            array('id' => $id,
                'account_id' => $request->input('account_id'),
                'name' => $request->input('name')),
            array(
                'id' => 'required|unique:projects,id,'.$id,
                'account_id' => 'required|exists:accounts,id',
                'bussiness_unit_id' => 'required|exists:bussiness_unit,id',
                'name'=> 'required|min:3'
                ));

        if($validator->fails()){
            return redirect()->route('getproject',[$id])->withErrors($validator)->withInput($request->all());
        }


        try{
            $project = Project::find($id);
            if(!$this->authorize('update',$project)){
                abort(403);
            }
            $project->name = $request->input('name');
            $project->bussiness_unit_id = $request->input('bussiness_unit_id');
            $project->created_by = $this->user()->id;
            $project->account_id = $request->input('account_id');
            $project->save();

        }catch(Exception $e){
            return redirect()->route('getproject',[$id])->with('status-error', trans('messages.db_error'))->withInput($request->all());     
        }
        
        return redirect()->route('getproject',[$id])->with('status-success', trans('messages.object_updated',['object'=>'Project']));   
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function apilist(Request $request)
    {

        if(!$this->authorize('lists',new Project)){
                abort(403);
            }

        $ret= array();
        if($request->query("account_id") != ""){
            $projects = Project::select('projects.id as id','projects.name as name')
            ->join('accounts','account_id','=','accounts.id')
            ->join('bussiness_unit','bussiness_unit_id','=','bussiness_unit.id')
            ->where('accounts.id','=',(int)$request->query("account_id"))
            ->get();
            $ret['data'] = $projects;
            $ret['draw'] = 1;
            $ret['recordsTotal'] = count($projects);
            $ret['recordsFiltered'] = count($projects); 
        }else{
            $ret['data'] = Project::select('id','name')->get();
            $ret['draw'] = 1;
            $ret['recordsTotal'] = count($ret['data']);
            $ret['recordsFiltered'] = count($ret['data']);       
        }

        return response()->json($ret);
    }
}
