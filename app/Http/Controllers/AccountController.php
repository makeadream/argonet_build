<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator;
use View;
use App\Models\Account;
use App\User;
use App\Models\Company;
use App\Models\AccountClassification;
use Gate;

class AccountController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function main()
    {

        if(!$this->authorize('menu-account')){
            abort(403);
        }
        
        return View::make('pages.frontoffice.account.main');
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function form($id = 0)
    {

        if(!$this->authorize('form', new Account)){
            abort(403);
        }

        $data = [   
        'action' => url('account'),
        'h1' => 'New Account'
        ];  

        if(0 != $id){
            $account = Account::find($id);

            if(!$account){
                Session::flash('status-error',trans('messages.no_object',['object' => 'Account']));
                return View::make('pages.frontoffice.account.form',$data);
            }

            if(!$this->authorize('read',$account)){
                Session::flash('status-error', trans('messages.no_permission'));
                return View::make('pages.frontoffice.account.form',$data);
            }

            $data = [   
            'action' => url('account/'.$id),
            'account' => $account,
            'h1' => 'Change Account'
            ];
        }

        $data['managers'] = User::all();
        $data['companies'] = Company::all();
        $data['classifications'] = AccountClassification::all();

        return View::make('pages.frontoffice.account.form',$data);
    }

     /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request, $id=0)
    {

        $data = [];
        
        //Verificar se veio ou não um valor no parametro $id
        if(0 == $id){

            if(!$this->authorize('create', new Account)){
                abort(403);
            }

            $validator = Validator::make(
                array(
                    'name' => $request->input('name'),
                    'company_id' => $request->input('company_id'),
                    'manager_id' => $request->input('manager_id'),
                    'account_classification_id' => $request->input('account_classification_id')
                    ),
                array(
                    'name'=> 'required|min:3',
                    'company_id'=> 'required|exists:companies,id',
                    'manager_id'=> 'required|exists:users,id',
                    'account_classification_id'=> 'required|exists:account_classifications,id'
                    ),
                array(
                    'name' => 'Name is not valid',
                    'company_id' => 'The Company is not valid',
                    'manager_id' => 'The Manager is not valid',
                    'account_classification_id' => 'The classification is not valid'
                    )
                );

            if($validator->fails()){
                return redirect('getaccount')->withErrors($validator);
            }

            try{
                Account::create([
                    'name' => $request->input('name'),
                    'created_by' => $this->user()->id,
                    'company_id' => $request->input('company_id'),
                    'manager_id' => $request->input('manager_id'),
                    'account_classification_id' => $request->input('account_classification_id'),
                    ]);
            }catch(Exception $e){
                return redirect()->route('getaccount',[$id])->with('status-error', trans('messages.db_error'));     
            }

            return redirect()->route('getaccount',[$id])->with('status-success', trans('messages.updated_object',['object' => 'Account'])); 
        }

        $validator = Validator::make(
            array(
                'id' => $id,
                'name' => $request->input('name'),
                'company_id' => $request->input('company_id'),
                'manager_id' => $request->input('manager_id'),
                'account_classification_id' => $request->input('account_classification_id')
                ),
            array(
                'id' => 'required|unique:accounts,id,'.$id,
                'name'=> 'required|min:3',
                'company_id'=> 'required|exists:companies,id',
                'manager_id'=> 'required|exists:users,id',
                'account_classification_id'=> 'required|exists:account_classifications,id',
                ),
            array(
                'id' => 'The company does not exists',
                'name' => 'Name is not valid',
                'company_id' => 'The Company is not valid',
                'manager_id' => 'The Manager is not valid',
                'account_classification' => 'The Classification is not valid',
                ));

        if($validator->fails()){
            return redirect()->route('getaccount',[$id])->withErrors($validator)->withInput($request->all());
        }

        try{
            $account = Account::find($id);
            if(!$this->authorize('update',$account)){
                abort(403);
            }

            $account->name = $request->input('name');
            $account->created_by = $user_id;
            $account->manager_id = $request->input('manager_id');
            $account->company_id = $request->input('company_id');
            $account->account_classification_id = $request->input('account_classification_id');
            $account->save();

        }catch(Exception $e){
            return redirect()->route('getaccount',[$id])->with('status-error', trans('messages.db_error'))->withInput($request->all());     
        }
        
        return redirect()->route('getaccount',[$id])->with('status-success', trans('messages.updated_object',['object' => 'Account']));   
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function apilist()
    {
        if(!$this->authorize('lists',new Account)){
            abort(403);
        }

        $ret['data'] = Account::
        select('accounts.id as id','accounts.name as name','users.name as manager','account_classifications.name as classification')
        ->leftJoin('users','manager_id','=','users.id')
        ->leftJoin('account_classifications','account_classification_id','=','account_classifications.id')
        ->get();
        $ret['draw'] = 1;
        $ret['recordsTotal'] = count($ret['data']);
        $ret['recordsFiltered'] = count($ret['data']);

        return response()->json($ret);
    }

}
