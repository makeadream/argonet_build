<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Models\Activity;
use App\Models\Event;
use App\Models\Project;
use App\Models\Account;
use DB;
use Exception;
use Validator;
use Auth;


class CalendarController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function main()
    {

        if(!$this->authorize('menu-calendar')){
            abort(403);
        }

        return View::make('pages.frontoffice.calendar.main');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!$this->authorize('lists',new Event)){
            abort(403);
        }

        return View::make('pages.frontoffice.calendar.calendar');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function calendar(Request $request)
    {

        if(!$this->authorize('lists',new Event)){
            abort(403);
        }

        $user_id = 1;
        $showdate = $request->input('showdate');
        $viewtype = $request->input('viewtype');
        $timezone = $request->input('');
        
        $dates = \App\Calendar::listCalendar($showdate, $viewtype);


        $ret = array();
        $ret['events'] = array();
        $ret["issort"] =true;
        $ret["start"] = \App\Calendar::php2JsTime($dates['start_time']);
        $ret["end"] = \App\Calendar::php2JsTime($dates['end_time']);
        $ret['error'] = null;



        $events = Event::select('events.id as id','subject','start_time','end_time','all_day','color')
        ->where('events.start_time','>=',$dates['start_time'])
        ->where('events.end_time','>=',$dates['end_time'])
        ->where('events.created_by','=',$this->user()->id)
        ->get();


        
        
        foreach($events as $event){
            $arr_event = [
            $event->id,
            $event->subject,
            \App\Calendar::Mysql2Js($event->start_time),
            \App\Calendar::Mysql2Js($event->end_time),
            $event->all_day,
            0,
            0,  //Recurring event
            (int)$event->color,
            1, //editable
            null, 
            ''//$attends
            ];

            array_push($ret['events'],$arr_event);
        }



        return response()->json($ret);
    }




    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function event(Request $request, $id=0)
    {


        if(!$this->authorize('form',new Event)){
            abort(403);
        }

        $accounts = Account::all();
        $activities = Activity::all();
        $projects = [];

        $data = [
        'accounts' => $accounts,
        'activities' => $activities,
        'account_id' => ''
        ];

        $url = url('calendar/event');
        $event = null;

        if($id){
            $event = Event::find($id);
            if(!$this->authorize('read',new Event)){
                abort(403);
            }
            $url .= "/".$event->id;


            if($event->project){
                $projects = Project::select('projects.id as id','projects.name as name')
                ->join('accounts','account_id','=','accounts.id')
                ->where('projects.id','=',$event->project->id)
                ->get();

                $data['account_id'] = $event->project->account_id;
            }
            
            $data['event'] = $event;
        }

        $data['url'] = $url;
        $data['projects'] = $projects;

        return View::make('pages.frontoffice.calendar.form',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function saveevent(Request $request,$id=0)
    {

        if($id){
            // Vou verificar se o utilizador tem permissão para aceder ao evento que está a consultar
            $event = Event::find($id);
            if(!$this->authorize('read',$event)){
                return response()->json(['IsSuccess'=> false, 'Msg'=> trans('messages.no_permission')]);
            }
        }

        $subject = $request->input('Subject');
        $color = (int)$request->input('colorvalue',-1); 

        //Converter as horas de Javascript para PHP
        $start_time = \App\Calendar::js2MySql($request->input('stpartdate')." ".$request->input('stparttime')) ;
        $end_time = \App\Calendar::js2MySql($request->input('etpartdate')." ".$request->input('etparttime')) ;

        $all_day = $request->input('IsAllDayEvent',0);
        $location = $request->input('Location');
        $description = $request->input('Description');
        $timezone = $request->input('timezone');

        $validator = Validator::make(
            array('subject' => $subject,
                'color' => $color,
                'all_day' => $all_day,
                'location' => $location,
                'description' => $description,
                'timezone' => $timezone
                ),
            array(
                'subject'=> 'required|min:3',
                'color' => 'in:-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13',
                'all_day' => 'boolean',
                'location' => 'string',
                'description' => 'string',
                'timezone' => 'string'
                ));

        if($validator->fails()){
            return response()->json(['IsSuccess'=> false, 'Msg'=>trans('messages.form_error')]);
        }

        $project_id = $request->input('Project');


        //Necessário validar se o projecto existe
        if($project_id)
        {
            $project = DB::table('projects')->where('id','=',(int)$project_id)->get();
            if(!$project){
                return response()->json(['IsSuccess'=> false, 'Msg'=>trans('messages.no_object',['object' => 'project'])]);
            }
        }

        //Necessário validar se a actividade existe
        $activity_id = $request->input('Activity');
        if($activity_id)
        {
            $activity = DB::table('activities')->where('id','=',(int)$project_id)->get();
            if(!$activity){
                return response()->json(['IsSuccess'=> false, 'Msg'=> trans('messages.no_object',['object'=>'Activity'])]);
            }
        }




        DB::beginTransaction();

        $ret = array();
        //try{
            $ret['Data'] = $start_time;

            $arr = [
            'subject' => trim($subject),
            'color' => $color,
            'start_time' => $start_time,
            'end_time' => $end_time,
            'all_day' => $all_day,
            'location' => trim($location),
            'description' => trim($description),
            'created_by' => $this->user()->id
            ];

            if($activity_id){
                $arr['activity_id'] = $activity_id;
            }

            if($project_id){
                $arr['project_id'] = $project_id;
            }

            if(isset($event)){
                if(!$this->authorize('update',$event)){
                    return response()->json(['IsSuccess'=> false, 'Msg'=> trans('messages.no_permission')]);
                }

                $event = Event::where('id','=',$id)
                ->update($arr);

                $ret['Msg'] = trans('messages.object_changed',['object'=>'Event']);
            }else{
                if(!$this->authorize('create',new Event)){
                    return response()->json(['IsSuccess'=> false, 'Msg'=> trans('messages.no_permission')]);
                }

                $event = Event::create($arr);
                $ret['Msg'] = trans('messages.object_created',['object'=>'Event']);
            }


            $ret['IsSuccess'] = true;
            

            DB::commit();  


       // } catch(\Exception $e)
        //{
         //   DB::rollback();
            $ret['IsSuccess'] = false;
            $ret['Msg'] = $e;
        //}


        return response()->json($ret);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function quickeventaction(Request $request,$id=0)
    {

        $eventId  = "";


        switch($request->query("method")){
            case "update":
            case "remove":  

            $id = $request->input('calendarId');
            try
            {
                    /*  
                    Validar o id e a existencia de um evento
                    */
                    if(!$id)
                    {
                        throw new Exception("Non id");   
                    }

                    $event = Event::find($id);
                    if(!$event)
                    {
                        throw new Exception("Event not found");
                    }
                }
                catch(Exception $e)
                {
                    return response()->json(["IsSuccess"=>false,"Msg"=>trans('messages.no_object',['object'=>'Event'])]);
                }

                if($request->query("method") == "update"){
                    if(!$this->authorize('update',$event)){
                        return response()->json(['IsSuccess'=> false, 'Msg'=> trans('messages.no_permission')]);
                    }

                    $event->start_time = \App\Calendar::js2MySql($request->input('CalendarStartTime'));
                    $event->end_time = \App\Calendar::js2MySql($request->input('CalendarEndTime'));
                    $event->save();
                    $msg = trans('messages.object_changed',['object'=>'Event']);
                }else{
                    if(!$this->authorize('destroy',$event)){
                        return response()->json(['IsSuccess'=> false, 'Msg'=> trans('messages.no_permission')]);
                    }

                    $event->delete();
                    $msg = trans('messages.object_removed',['object'=>'Event']);
                }

                break;
                case "add":
                if(!$this->authorize('create',$event)){
                    return response()->json(['IsSuccess'=> false, 'Msg'=> trans('messages.no_permission')]);
                }

                $subject = $request->input('CalendarTitle');
                $start_time = \App\Calendar::js2MySql($request->input('CalendarStartTime'));
                $end_time = \App\Calendar::js2MySql($request->input('CalendarEndTime'));
                $event = Event::create([
                    'subject' => trim($subject),
                    'color' => -1,
                    'start_time' => $start_time,
                    'end_time' => $end_time,
                    'all_day' => 0,
                    'created_by' => $this->user()->id
                    ]);
                $msg = trans('messages.object_created',['object'=>'Event']);
                $eventId =  $event->id;
                break;
            }

            return response()->json(["IsSuccess"=>true,"Msg"=>$msg,"Data"=>$eventId]);
        }


    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function validationlist()
    {

        if(!$this->authorize('validationlist',new Event)){
            return response()->json(['IsSuccess'=> false, 'Msg'=> trans('messages.no_permission')]);
        }

        return View::make('pages.frontoffice.calendar.validationlist');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function apivalidationlist()
    {

        if(!$this->authorize('validationlist',new Event)){
            return response()->json(['IsSuccess'=> false, 'Msg'=> trans('messages.no_permission')]);
        }

        $ret['data'] = Event::
        select('events.id as id','events.subject as subject','projects.name as project','accounts.name as account')
        ->leftJoin('projects','projects.id','=','events.id')
        ->leftJoin('accounts','accounts.id','=','projects.account_id')
        ->where('state','=','CREATED')
        ->get();
        $ret['draw'] = 1;
        $ret['recordsTotal'] = count($ret['data']);
        $ret['recordsFiltered'] = count($ret['data']);

        return response()->json($ret);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getvalidationform(Request $request,$id=0)
    {

        $event = Event::find($id);
        if(!$this->authorize('validationform',new Event)){
            return response()->json(['IsSuccess'=> false, 'Msg'=> trans('messages.no_permission')]);
        }
        
        $data['event'] = $event;

        return View::make('pages.frontoffice.calendar.validationform',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postvalidationform(Request $request,$id=0)
    {

        if(!$this->authorize('validationform',new Event)){
            return response()->json(['IsSuccess'=> false, 'Msg'=> trans('messages.no_permission')]);
        }

        $event = Event::find($id);

        switch($request->input('state')){
            case 'VALIDATED':
            $event->validate();
            Session::flash('status-success', trans('messages.success_validate_event'));
            break;
            case 'NOT_VALIDATED':
            $event->novalidate();
            Session::flash('status-success', trans('messages.success_no_validate_event'));
            break;
            default:
            abort(403);
            break;
        }

        return View::make('pages.frontoffice.calendar.validationlist',$data);
    }


}