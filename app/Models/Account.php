<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $fillable = ['name','created_by','manager_id','company_id','account_classification_id'];

    public function manager()
    {
        return $this->hasOne('App\User','manager_id','id');
    }

    public function company()
    {
        return $this->hasOne('App\Models\Company');
    }
}
