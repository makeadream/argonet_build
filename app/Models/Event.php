<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use ACL;
use StateMachine;
use Auth;


class Event extends Model
{


    protected $fillable = [ 'subject','color' ,'start_time','end_time','all_day' ,
    'location' ,'description','created_by' ,'project_id','actitivy_id'];

 	/**
     * Um evento de calendário tem sempre uma actividade
     */
    public function activity()
    {
        return $this->hasOne('App\Models\Activity');
    }


	/**
     * Um pode ter um projecto
     */
    public function project()
    {
        return $this->hasOne('App\Models\Project','id','project_id');
    }

    /**
     * O event_to tem sempre de estar associado ao seu evento de calendário
     */
    public function to()
    {
        return $this->hasMany('App\Models\Event_to');
    } 

    /**
     * O event_cc tem sempre de estar associado ao seu evento de calendário
     */
    public function cc()
    {
        return $this->hasMany('App\Models\Event_cc');
    }     

    /**
     * O event_bcc tem sempre de estar associado ao seu evento de calendário
     */
    public function bcc()
    {
        return $this->hasMany('App\Models\Event_bcc');
    }  

    public function getStartDateInJS(){
        $date = explode(" ",$this->start_time);
        $date = explode("-",$date[0]);
        return $date[1]."/".$date[2]."/".$date[0];
    }

    public function getStartTimeInJS(){
        $date = explode(" ",$this->start_time);
        $time = explode(":",$date[1]);
        return $time[0].":".$time[1];
    }

    public function getEndDateInJS(){
        $date = explode(" ",$this->end_time);
        $date = explode("-",$date[0]);
        return $date[1]."/".$date[2]."/".$date[0];
    }

    public function getEndTimeInJS(){
        $date = explode(" ",$this->end_time);
        $time = explode(":",$date[1]);
        return $time[0].":".$time[1];
    }

    public static function create(array $attributes = array()){

        DB::beginTransaction();
        try{
            $arguments['status'] = 'CREATED';
            
            if(!StateMachine::check(get_class(self),null,$arguments['status'])){
                return false;    
            }
            
            $results = parent::create($arguments);

            $context = ['user' => Auth::user(),'event' => $results];
            StateMachine::trigger(self::$workflow,null,$arguments['status'],$context);
            DB::commit();
        }catch(Exception $e){
            DB::rollback();
        }
        
        return $results;
    }

    public function validate($arguments = []){
        DB::beginTransaction();
        try{
            $arguments['status'] = 'VALIDATED';
            $context = ['user' => Auth::user(),'event' => $this];
            StateMachine::trigger(get_class($this),$this->status,$arguments['status'],$context);

            $results = $this->update($arguments);
            DB::commit();
        }catch(Exception $e){
            DB::rollback();
        }
        return $results;
    }

    public function novalidate($arguments = []){
        DB::beginTransaction();
        try{    
            $arguments['status'] = 'NOT_VALIDATED';    
            $context = ['user' => Auth::user(),'event' => $this];

            $oldStatus = $this->status;
            $this->update($arguments);
            StateMachine::trigger(get_class($this),$oldStatus,$arguments['status'],$context);
            
            DB::commit();
        }catch(Exception $e){
            DB::rollback();
        }
        return $results;
    }
}