<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StateTrigger extends Model
{
    
    public function trigger(){
    	return $this->hasOne('App\Models\Trigger','id','trigger_id');
    }
}
