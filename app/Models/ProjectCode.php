<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectCode extends Model
{
    protected $fillable = ['name','created_by','project_id'];
}
