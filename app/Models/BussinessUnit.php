<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BussinessUnit extends Model
{
    protected $fillable = ['name','created_by'];
}
