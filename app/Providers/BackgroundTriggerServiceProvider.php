<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BackgroundTriggerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('BackgroundTrigger', function() {
            return new \App\Custom\BackgroundTrigger();
        });
    }
}
