<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use ACL;
use Auth;
use App\Models\Action;

class AuthServiceProvider extends ServiceProvider
{
    /**
    *
    **/


    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
    'App\Model' => 'App\Policies\ModelPolicy',
    'App\Models\Account' => 'App\Policies\AccountPolicy',
    'App\Models\Company' => 'App\Policies\CompanyPolicy',
    'App\Models\Project' => 'App\Policies\ProjectPolicy',
    'App\Models\Event' => 'App\Policies\EventPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        parent::registerPolicies($gate);

     //   $this->bootMicrosoftSocialite();
      /* $actions = Action::all();
        foreach($actions as $action){
            $permission = $action->name;
            $gate->define($permission, function ($user) use($permission){
                return ACL::check($permission,$user);   
            });
        }
       */
        
    }

    private function bootMicrosoftSocialite()
    {
        $socialite = $this->app->make('Laravel\Socialite\Contracts\Factory');
        $socialite->extend(
            'microsoft',
            function ($app) use ($socialite) {
                $config = $app['config']['services.microsoft'];
                return $socialite->buildProvider(MicrosoftProvider::class, $config);
            }
            );
    }

}
