<?php
/**
* @author Francisco Menéres
* 
*/

namespace App\Custom;

use DB;
use App\Models\State;
use App\Models\WorkFlow;
use App\Models\StateTrigger;
use BackgroundTrigger;

class StateMachine {


    /**
    * 
    * Check in the database if the user has permission for the action
    * 
    * @param int $wf
    * @param string $start_state 
    * @param string $end_state 
    * @return boolean
    **/
    private function validate( $wf = "", $start_state = "", $end_state = "")
    {
        
        $result = State::where('workflow','like',$wf)
        ->where('start_state','=',$start_state)
        ->where('end_state','=',$end_state)
        ->first();

        return $result;
    } 


	/**
	* 
	* Check in the database if the user has permission for the action
	* 
	* @param int $wf
	* @param string $start_state 
    * @param string $end_state 
	* @return boolean
	**/
    public function check( $wf = "", $start_state = "", $end_state = "")
    {
        
    	$result = $this->validate($wf, $start_state, $end_state);

        return ($result)?true:false;
    }

    /**
    * 
    * Com base na máquina de estados activa o trigger se existir
    * 
    * @param int $wf
    * @param string $start_state 
    * @param string $end_state 
    **/
    public function trigger( $wf = "", $start_state = "", $end_state = "",$context)
    {

        
        $result = $this->validate($wf, $start_state, $end_state);
        if(!$result){
            abort(403);
        }

        $sts = StateTrigger::where('state_id','=',$result->id)->get();

        if(!count($sts)){
            return false;
        }

        foreach($sts as $st){
            BackgroundTrigger::execute($st->trigger,$context);
        }

        return true;
    }

}