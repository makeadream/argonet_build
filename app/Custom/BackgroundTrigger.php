<?php
/**
* @author Francisco Menéres
* 
*/

namespace App\Custom;

use DB;
use App\User;
use Mail;
use Log;
use App\Models\Trigger;

class BackgroundTrigger {


	public function execute(Trigger $trigger,$context)
	{

		$method = $trigger->name;
        $this->$method($context);
		return false;
	}


	public function sendInvitation($context){

		if(!isset($context['user']) || !($context['user'] instanceof User)){
			Log::error('No object user to extract information do send invitation');
			return false;
		}
$data = [];
		Mail::send('emails.invitation', $data, function ($message) {
			$message->from('francisco.meneres@gmail.com', 'Laravel');
			$message->to('franciscomeneres@makeadream.pt')->subject('teste');
		});

	}
}