<?php
/**
* @author Francisco Menéres
* 
*/

namespace App\Custom;

use DB;
use App\User;

class ACL {

	/**
	* 
	* Check in the database if the user has permission for the action
	* 
	* @param User $user_id
	* @param string $action 
	* @return boolean
	**/
    public function check($action = "",User $user)
    {

    	$result = DB::table('role_users')
    	->leftjoin('role_actions','role_actions.role_id','=','role_users.role_id')
    	->leftjoin('actions', 'actions.id','=','role_actions.action_id')
    	->where('role_users.user_id','=',$user->en_talent_c)
    	->where('actions.name','=',$action)
    	->count();

        return ($result != 0)?true:false;
    }
}