<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Event;
use ACL;
use App\User;

class EventPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function create(User $user){
        return ACL::check('create-event',$user);
    }

    /**
    * Método para verificar a permissão de actualizar a informação
    *
    * @param User $user
    * @param App\Models\Event $event
    * @return bool
    **/
    public function update(User $user,Event $event){

        // Pode fazer a actualização se tiver permissão e se for o criador
        if(
            ACL::check('update-event',$user) 
            && 
            $event->created_by == $user->id
        ){
            return true;
        }

        return false;
    }

    /**
    * Método para verificar a permissão de ler a informação
    *
    * @param User $user
    * @param App\Models\Event $event
    * @return bool
    **/
    public function read(User $user,Event $event){
        return ACL::check('read-event',$user) ;
    }

    public function lists(User $user){
        return ACL::check('lists-event',$user) ;
    }

    public function form(User $user){
        return ACL::check('form-event',$user) ;
    }

    public function validationlist(User $user,Event $event){
        return ACL::check('validationlist-event',$user) ;
    }

    public function validate(User $user,Event $event){
        return ACL::check('validate-event',$user) ;        
    }

    public function validationform(User $user,Event $event){
        return ACL::check('validationform-event',$user) ;
    }

    public function destroy(User $user,Event $event){
        // Pode fazer a remoção se tiver permissão e se for o criador
        if(
            ACL::check('destroy-event',$user) 
            && 
            $event->created_by == $user->id
        ){
            return true;
        }

        return false;
    }
}

