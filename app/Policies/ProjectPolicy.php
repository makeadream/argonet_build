<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Project;
use ACL;
use App\User;

class ProjectPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function create(User $user){
        return ACL::check('create-project',$user);
    }

    /**
    * Método para verificar a permissão de actualizar a informação
    *
    * @param User $user
    * @param App\Models\Account $account
    * @return bool
    **/
    public function update(User $user,Project $project){

        // Pode fazer a actualização se tiver permissão e se for o criador
        if(
            ACL::check('update-project',$user) 
            && 
            $project->created_by == $user->id
        ){
            return true;
        }

        return false;
    }

    /**
    * Método para verificar a permissão de ler a informação
    *
    * @param User $user
    * @param App\Models\Project $project
    * @return bool
    **/
    public function read(User $user,Project $project){
        return ACL::check('read-project',$user) ;
    }

    public function lists(User $user){
        return ACL::check('lists-project',$user) ;
    }

    public function form(User $user){
        return ACL::check('form-project',$user) ;
    }

    public function destroy(User $user,Project $project){
        // Pode fazer a remoção se tiver permissão e se for o criador
        if(
            ACL::check('destroy-project',$user) 
            && 
            $account->created_by == $user->id
        ){
            return true;
        }

        return false;
    }

}
