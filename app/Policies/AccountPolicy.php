<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Account;
use DB;
use Auth;
use ACL;
use App\User;

class AccountPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function before($user, $ability)
    {
        if (Auth::user()->isSuperAdmin()) {
            return true;
        }
    }

    public function create(User $user){
        return ACL::check('create-account',$user);
    }

    /**
    * Método para verificar a permissão de actualizar a informação
    *
    * @param User $user
    * @param App\Models\Account $account
    * @return bool
    **/
    public function update(User $user,Account $account){

        // Pode fazer a actualização se tiver permissão e se for o manager da Account
        if(
            ACL::check('update-account',$user->id) 
            && 
            $account->manager_id == $user->id
        ){
            return true;
        }

        return false;
    }

    /**
    * Método para verificar a permissão de ler a informação
    *
    * @param User $user
    * @param App\Models\Account $account
    * @return bool
    **/
    public function read(User $user,Account $account){
        return ACL::check('read-account',$user);
    }

    public function lists(User $user){
        return ACL::check('lists-account',$user);
    }

    public function form(User $user){
        return ACL::check('form-account',$user);
    }

    public function destroy(User $user,Account $account){
        // Pode fazer a remoção se tiver permissão e se for o manager da Account
        if(
            ACL::check('destroy-account',$user->id) 
            && 
            $account->manager_id == $user->id
        ){
            return true;
        }

        return false;
    }
}
