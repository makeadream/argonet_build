<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use App\Models\Company;
use ACL;
use App\User;

class CompanyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }


    public function before($user, $ability)
    {
        if ($user->isSuperAdmin()) {
            return true;
        }
    }

    public function create(User $user){
        return ACL::check('create-company',$user);
    }

    /**
    * Método para verificar a permissão de actualizar a informação
    *
    * @param User $user
    * @param App\Models\Company $company
    * @return bool
    **/
    public function update(User $user,Company $company){

        // Pode fazer a actualização se tiver permissão e se for o criador
        if(
            ACL::check('update-company',$user) 
            && 
            $company->created_by == $user->id
        ){
            return true;
        }

        return false;
    }

    /**
    * Método para verificar a permissão de ler a informação
    *
    * @param User $user
    * @param App\Models\Company $company
    * @return bool
    **/
    public function read(User $user,Company $company){

        return ACL::check('read-company',$user) ;
    }

    public function lists(User $user){
        return ACL::check('lists-company',$user) ;
    }

    public function form(User $user){

        return ACL::check('form-company',$user) ;
    }

    public function destroy(User $user,Company $company){
        // Pode fazer a remoção se tiver permissão e se for o criador
        if(
            ACL::check('destroy-company',$user) 
            && 
            $company->created_by == $user->id
        ){
            return true;
        }

        return false;
    }

}
