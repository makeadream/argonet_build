<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject',500);
            $table->string('description',500)->nullable();
            $table->timestamp('start_time');
            $table->timestamp('end_time');
            $table->boolean('all_day')->default(0);
            $table->string('location',500)->nullable();
            $table->string('color',6)->nullable();
            $table->string('recurring_rule',500)->nullable();
            $table->integer('project_id')->unsigned();
            $table->integer('activity_id')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->integer('workflow_id')->unsigned();
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('events');
    }
}
