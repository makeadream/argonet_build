<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBussinessUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bussiness_unit', function (Blueprint $table) {
            $table->increments('cd_unit');
            $table->string('designacao',255);
            $table->date('date');
            $table->integer('en_manager')->unsigned();
            $table->string('cod',255);
            $table->string('designacao2',255);
            $table->integer('competencie_id')->unsigned();
            $table->integer('en_manager2')->unsigned();
           // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bussiness_unit');
    }
}
