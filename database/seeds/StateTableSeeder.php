<?php

use Illuminate\Database\Seeder;

class StateTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('states')->insert([
            'workflow'=>'Event',
            'start_state' => null,
            'end_state' => 'CREATED'
            ]);

        DB::table('states')->insert([
            'workflow'=>'Event',
            'start_state' => 'CREATED',
            'end_state' => 'CREATED'
            ]);

        DB::table('states')->insert([
            'workflow'=>'Event',
            'start_state' => 'CREATED',
            'end_state' => 'VALIDATED'
            ]);

        DB::table('states')->insert([
            'workflow'=>'Event',
            'start_state' => 'CREATED',
            'end_state' => 'NOT_VALIDATED'
            ]);

    }
}
