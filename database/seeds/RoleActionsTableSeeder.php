<?php

use Illuminate\Database\Seeder;
use App\Models\Action;

class RoleActionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$now = date("Y-m-d H:m:s");
    	$actions = Action::all();
    	foreach($actions as $action){
    		DB::table('role_actions')->insert([
            'action_id'=> $action->id,
            'role_id' => 1,
            'created_at' => $now
            ]);
    	}
        
    }
}
