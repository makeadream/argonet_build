<?php

use Illuminate\Database\Seeder;

class ActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('activities')->insert([
            'name' => 'Kick-off e Set-up',
        ]);
        DB::table('activities')->insert([
            'name' => 'Diagnóstico',
        ]);
        DB::table('activities')->insert([
            'name' => 'Conceção e design',
        ]);
        DB::table('activities')->insert([
            'name' => 'Implementação',
        ]);
        DB::table('activities')->insert([
            'name' => 'Fecho e avaliação',
        ]);
    }
}
