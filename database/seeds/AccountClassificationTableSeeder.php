<?php

use Illuminate\Database\Seeder;

class AccountClassificationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('account_classifications')->insert([
            'name'=>'Key Account'
            ]);
        DB::table('account_classifications')->insert([
            'name'=>'Medium Account'
            ]);
        DB::table('account_classifications')->insert([
            'name'=>'Low Account'
            ]);
    }
}
