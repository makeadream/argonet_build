<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = date("Y-m-d H:m:s");
        DB::table('roles')->insert([
            'name'=>'Partner',
            'created_at' => $now
            ]);
        DB::table('roles')->insert([
            'name'=>'RC',
            'created_at' => $now
            ]);
        DB::table('roles')->insert([
            'name'=>'Manager',
            'created_at' => $now
            ]);
        DB::table('roles')->insert([
            'name'=>'User',
            'created_at' => $now
            ]);
    }
}
