<?php

use Illuminate\Database\Seeder;

class ActionTableSeeder extends Seeder
{

	protected $actions = [
		'create-account',
		'update-account',
		'read-account',
		'destroy-account',
        'form-account',
        'lists-account',

        'create-company',
        'update-company',
        'read-company',
        'destroy-company',
        'form-company',
        'lists-company',

        'create-event',
        'update-event',
        'read-event',
        'destroy-event',
        'form-event',
        'lists-event',
        'validationlist-event',
        'validate-event',
        'validationform-event',

        'create-project',
        'update-project',
        'read-project',
        'destroy-project',
        'form-project',
        'lists-project',

        'menu-dashboard',
        'menu-company',
        'menu-account',
        'menu-project',
        'menu-calendar',
	];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	foreach($this->actions as $action){
        	DB::table('actions')->insert([
            	'name' => $action,
        	]);
    	}

    }

}
